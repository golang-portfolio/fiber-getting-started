migrateup:
	migrate -path db/migrations -database "postgresql://elites_user:elites_pass@localhost:5432/fiber_demo?sslmode=disable"  force 1
	migrate -path db/migrations -database "postgresql://elites_user:elites_pass@localhost:5432/fiber_demo?sslmode=disable" -verbose up

migratedown:
	migrate -path db/migrations -database "postgresql://elites_user:elites_pass@localhost:5432/fiber_demo?sslmode=disable"  force 1
	migrate -path db/migrations -database "postgresql://elites_user:elites_pass@localhost:5432/fiber_demo?sslmode=disable" -verbose down