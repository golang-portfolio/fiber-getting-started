package common

import (
	"time"
	"github.com/google/uuid"
  )

type Base struct {
	ID     			uuid.UUID 	`gorm:"primary_key;unique;type:uuid;column:id" json:"id"`
	CreatedOn		time.Time 	`gorm:"autoCreateTime:true" gorm:"autoUpdateTime:false" json:"created_on"`
	UpdatedOn		time.Time 	`gorm:"autoCreateTime:true" json:"updated_on"`
	DeletedOn		*time.Time 	`json:"deleted_on"`
	CreatedBy		string 		`json:"created_by"`
	UpdatedBy		string  	`json:"updated_by"`
    IsActive        bool
}

type BaseModel struct {
	Base
	InstitutionCode string		`json:"institution_code"`
}
