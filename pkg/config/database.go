package config

import (
	"gorm.io/gorm"
	"gorm.io/driver/postgres"
	// "log"

	// "github.com/golang-migrate/migrate/v4"
	// _ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

var DB *gorm.DB

func ConnectToDb() {
	var err error
	dbURL := "postgres://elites_user:elites_pass@localhost:5432/fiber_demo"
	// m, err := migrate.New(
	// 	"file://db/migrations", dbURL)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// if err := m.Up(); err != nil {
	// 	log.Fatal(err)
	// }

	DB, err = gorm.Open(postgres.Open(dbURL), &gorm.Config{})
	if err != nil {
		panic("Failed to connect database")
	}
}