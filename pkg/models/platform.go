package models

import (
	"github.com/Kengathua/fiberone/pkg/common"
	"github.com/google/uuid"
  )

type Institution struct {
	common.Base
	InstitutionName 	string 		`json:"institution_name"`
	InstitutionCode 	string		`json:"institution_code"`
	Description 		string		`json:"description"`
}

type School struct {
	common.BaseModel
	InstitutionID 		uuid.UUID	`gorm:"foreignKey:ID" json:"institution_id"`
	Institution			Institution	`gorm:"foreignkey:InstitutionID;references:id" json:"institution"`
	SchoolName 			string 		`json:"school_name"`
	SchoolCode 			string		`json:"school_code"`
	Description 		string		`json:"description"`
}

type Programme struct {
	common.BaseModel
	InstitutionID 		uuid.UUID	`gorm:"foreignKey:ID" json:"institution_id"`
	Institution			Institution	`gorm:"foreignkey:InstitutionID;references:id" json:"institution"`
	ProgrammeName 		string 		`json:"programmes_name"`
	ProgrammeCode 		string		`json:"programmes_code"`
	Description 		string		`json:"description"`
}

type Level struct {
	common.BaseModel
	InstitutionID 		uuid.UUID	`gorm:"foreignKey:ID" json:"institution_id"`
	Institution			Institution	`gorm:"foreignkey:InstitutionID;references:id" json:"institution"`
	LevelName 			string 		`json:"level_name"`
	LevelCode 			string		`json:"level_code"`
	Description 		string		`json:"description"`
}

type SubLevel struct {
	common.BaseModel
	LevelID 			uuid.UUID	`gorm:"foreignKey:ID" json:"level_id"`
	Level				Level		`gorm:"foreignkey:LevelID;references:id" json:"level"`
	SubLevelName 		string 		`json:"sub_level_name"`
	SubLevelCode 		string		`json:"sub_level_code"`
	Semester			string		`json:"semester"`
	Description 		string		`json:"description"`
}

type Department struct {
	common.BaseModel
	SchoolID 			uuid.UUID	`gorm:"foreignKey:ID" json:"school_id"`
	School				School		`gorm:"foreignkey:SchoolID;references:id" json:"school"`
	DepartmentName 		string 		`json:"department_name"`
	DepartmentCode 		string		`json:"department_code"`
	Description 		string		`json:"description"`
}

type Subject struct {
	common.BaseModel
	DepartmentID 		uuid.UUID		`gorm:"foreignKey:ID" json:"department_id"`
	Department			Department		`gorm:"foreignkey:DepartmentID;references:id" json:"department"`
	SubjectName 		string 			`json:"subject_name"`
	SubjectCode 		string			`json:"subject_code"`
	Description 		string			`json:"description"`
	Programmes 			[]*Programme	`gorm:"many2many:subject_programme;"`
}

type SubjectProgramme struct {
	common.BaseModel
	SubjectID 			uuid.UUID	`gorm:"foreignKey:ID" json:"subject_id"`
	Subject				Subject		`gorm:"foreignkey:SubjectID;references:id" json:"subject"`
	ProgrammeID 		uuid.UUID	`gorm:"foreignKey:ID" json:"programme_id"`
	Programme			Programme	`gorm:"foreignkey:ProgrammeID;references:id" json:"programme"`
}

type Unit struct {
	common.BaseModel
	DepartmentID 		uuid.UUID		`gorm:"foreignKey:ID" json:"department_id"`
	Department			Department		`gorm:"foreignkey:DepartmentID;references:id" json:"department"`
	UnitName 			string 			`json:"unit_name"`
	UnitCode 			string			`json:"unit_code"`
	SecondaryDepartments []Department	`gorm:"many2many:unit_secondary_department;"`
}

type UnitSecondaryDepartments struct{
	common.BaseModel
	DepartmentID 		uuid.UUID		`gorm:"foreignKey:ID" json:"department_id"`
	Department			Department		`gorm:"foreignkey:DepartmentID;references:id" json:"department"`
	UnitId				uuid.UUID		`json:"unit_id"`
	Unit				Unit			`gorm:"foreignkey:UnitID;references:id" json:"unit"`
}

type Course struct {
	common.BaseModel
	SubjectID 			uuid.UUID	`gorm:"foreignKey:ID" json:"subject_id"`
	Subject				Subject		`gorm:"foreignkey:SubjectID;references:id" json:"subject"`
	ProgrammeID 		uuid.UUID	`gorm:"foreignKey:ID" json:"programme_id"`
	Programme			Programme	`gorm:"foreignkey:ProgrammeID;references:id" json:"programme"`
    CourseCode			string		`json:"course_code"`
	Units				[]Unit		`gorm:"many2many:course_unit;"`
}

type CourseUnit	struct {
	common.BaseModel
	CourseId			uuid.UUID	`gorm:"foreignKey:ID" json:"course_id"`
	Course				Course		`gorm:"foreignkey:CourseID;references:id" json:"course"`
	UnitId				uuid.UUID	`gorm:"foreignKey:ID" json:"unit_id"`
	Unit				Unit		`gorm:"foreignkey:UnitID;references:id" json:"unit"`
	LevelID 			uuid.UUID	`gorm:"foreignKey:ID" json:"level_id"`
	Level				Level		`gorm:"foreignkey:LevelID;references:id" json:"level"`
	SubLevelID 			uuid.UUID	`gorm:"foreignKey:ID" json:"sub_level_id"`
	SubLevel			SubLevel	`gorm:"foreignkey:SubLevelID;references:id" json:"sub_level"`
}

type Student struct {
	common.BaseModel
	FirstName			string
	OtherNames			string
	LastName			string
	CourseId			uuid.UUID	`gorm:"foreignKey:ID" json:"course_id"`
	Course				Course		`gorm:"foreignkey:CourseID;references:id" json:"course"`
	StudentNumber		string
	Email				string
	Phone				string
	// Data				map[string]interface{} `gorm:"serializer:json"`
}

type StudentSession struct {
	StudentId			uuid.UUID	`gorm:"foreignKey:ID" json:"student_id"`
	Student				Student		`gorm:"foreignkey:StudentID;references:id" json:"student"`
	AcademicYear    	string
    Semester        	string
    MajorUnits      	[]Unit
    OtherUnits      	[]Unit
}

// Unit
// SecondaryDepartmentUnit
// CourseUnit
// Student
// StudentSessionData
// LecturerLecture
// SemesterUnit
// SemesterUnitLecturer
// Class
// Buildings
// Lecture Halls
// Lecture