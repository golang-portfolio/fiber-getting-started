package routes

import (
	"github.com/gofiber/fiber/v2"
    "github.com/Kengathua/fiberone/pkg/controllers"
	"gorm.io/gorm"
)


func RegisterRoutes(url fiber.Router, db *gorm.DB) {
    h := &controllers.Handler{
        DB: db,
    }

    institutionRoutes := url.Group("/institutions")
    institutionRoutes.Post("/", h.AddInstitution)
    institutionRoutes.Get("/", h.GetInstitutions)
    institutionRoutes.Get("/:id", h.GetInstitution)
    institutionRoutes.Put("/:id", h.UpdateInstitution)
    institutionRoutes.Delete("/:id", h.DeleteInstitution)

	schoolRoutes := url.Group("/schools")
	schoolRoutes.Post("/", h.AddSchool)
	schoolRoutes.Get("/", h.GetSchools)
	schoolRoutes.Get("/:id", h.GetSchool)
	schoolRoutes.Put("/:id", h.UpdateSchool)
	schoolRoutes.Delete("/:id", h.DeleteSchool)

	programmeRoutes := url.Group("/programmes")
	programmeRoutes.Post("/", h.AddProgramme)
	programmeRoutes.Get("/", h.GetProgrammes)
	programmeRoutes.Get("/:id", h.GetProgramme)
	programmeRoutes.Put("/:id", h.UpdateProgramme)
	programmeRoutes.Delete("/:id", h.DeleteProgramme)

	levelRoutes := url.Group("/levels")
	levelRoutes.Post("/", h.AddLevel)
	levelRoutes.Get("/", h.GetLevels)
	levelRoutes.Get("/:id", h.GetLevel)
	levelRoutes.Put("/:id", h.UpdateLevel)
	levelRoutes.Delete("/:id", h.DeleteLevel)

	subLevelRoutes := url.Group("/sub_levels")
	subLevelRoutes.Post("/", h.AddSubLevel)
	subLevelRoutes.Get("/", h.GetSubLevels)
	subLevelRoutes.Get("/:id", h.GetSubLevel)
	subLevelRoutes.Put("/:id", h.UpdateSubLevel)
	subLevelRoutes.Delete("/:id", h.DeleteSubLevel)
}