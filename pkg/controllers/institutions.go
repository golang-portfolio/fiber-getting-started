package controllers

import (
	"github.com/gofiber/fiber/v2"
    "github.com/Kengathua/fiberone/pkg/models"
)

type InstitutionRequestBody struct {
    InstitutionName		string `json:"institution_name"`
    InstitutionCode		string `json:"institution_code"`
    Description 		string `json:"description"`
}

func (h Handler) GetInstitutions(c *fiber.Ctx) error {
    var institutions []models.Institution

    if result := h.DB.Find(&institutions); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusOK).JSON(&institutions)
}

func (h Handler) GetInstitution(c *fiber.Ctx) error {
    id := c.Params("id")

    var institution models.Institution

    if result := h.DB.First(&institution, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusOK).JSON(&institution)
}

func (h Handler) AddInstitution(c *fiber.Ctx) error {
    body := InstitutionRequestBody{}

    // parse body, attach to InstitutionRequestBody struct
    if err := c.BodyParser(&body); err != nil {
        return fiber.NewError(fiber.StatusBadRequest, err.Error())
    }

    var institution models.Institution

    institution.InstitutionName = body.InstitutionName
    institution.InstitutionCode = body.InstitutionCode
    institution.Description = body.Description

    // insert new db entry
    if result := h.DB.Create(&institution); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusCreated).JSON(&institution)
}

func (h Handler) UpdateInstitution(c *fiber.Ctx) error {
    id := c.Params("id")
    body := InstitutionRequestBody{}

    // getting request's body
    if err := c.BodyParser(&body); err != nil {
        return fiber.NewError(fiber.StatusBadRequest, err.Error())
    }

    var institution models.Institution

    if result := h.DB.First(&institution, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    institution.InstitutionName = body.InstitutionName
    institution.InstitutionCode = body.InstitutionCode
    institution.Description = body.Description

    // save institution
    h.DB.Save(&institution)

    return c.Status(fiber.StatusOK).JSON(&institution)
}

func (h Handler) DeleteInstitution(c *fiber.Ctx) error {
    id := c.Params("id")

    var institution models.Institution

    if result := h.DB.First(&institution, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    // delete institution from db
    h.DB.Delete(&institution)

    return c.SendStatus(fiber.StatusOK)
}