package controllers

import (
	"github.com/google/uuid"
	"github.com/gofiber/fiber/v2"
    "github.com/Kengathua/fiberone/pkg/models"
)

type SchoolRequestBody struct {
	InstitutionID	uuid.UUID	`json:"institution_id"`
	InstitutionCode	string		`json:"institution_code"`
    SchoolName		string 		`json:"school_name"`
    SchoolCode		string 		`json:"school_code"`
    Description 	string 		`json:"description"`
}

func (h Handler) GetSchools(c *fiber.Ctx) error {
    var schools []models.School

    if result := h.DB.Preload("Institution").Find(&schools); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusOK).JSON(&schools)
}

func (h Handler) GetSchool(c *fiber.Ctx) error {
    id := c.Params("id")

    var school models.School

    if result := h.DB.Preload("Institution").First(&school, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusOK).JSON(&school)
}

func (h Handler) AddSchool(c *fiber.Ctx) error {
    body := SchoolRequestBody{}

    // parse body, attach to SchoolRequestBody struct
    if err := c.BodyParser(&body); err != nil {
        return fiber.NewError(fiber.StatusBadRequest, err.Error())
    }

    var school models.School

	school.InstitutionCode = body.InstitutionCode
	school.InstitutionID = body.InstitutionID
    school.SchoolName = body.SchoolName
    school.SchoolCode = body.SchoolCode
    school.Description = body.Description

    // insert new db entry
    if result := h.DB.Create(&school); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusCreated).JSON(&school)
}

func (h Handler) UpdateSchool(c *fiber.Ctx) error {
    id := c.Params("id")
    body := SchoolRequestBody{}

    // getting request's body
    if err := c.BodyParser(&body); err != nil {
        return fiber.NewError(fiber.StatusBadRequest, err.Error())
    }

    var school models.School

    if result := h.DB.Preload("Institution").First(&school, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

	school.InstitutionCode = body.InstitutionCode
	school.InstitutionID = body.InstitutionID
    school.SchoolName = body.SchoolName
    school.SchoolCode = body.SchoolCode
    school.Description = body.Description

    // save school
    h.DB.Save(&school)

    return c.Status(fiber.StatusOK).JSON(&school)
}

func (h Handler) DeleteSchool(c *fiber.Ctx) error {
    id := c.Params("id")

    var school models.School

    if result := h.DB.First(&school, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    // delete school from db
    h.DB.Delete(&school)

    return c.SendStatus(fiber.StatusOK)
}