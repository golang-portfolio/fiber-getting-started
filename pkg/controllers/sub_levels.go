package controllers

import (
	"github.com/google/uuid"
	"github.com/gofiber/fiber/v2"
    "github.com/Kengathua/fiberone/pkg/models"
)

type SubLevelRequestBody struct {
	LevelID         uuid.UUID `json:"level_id"`
	InstitutionCode string    `json:"institution_code"`
	SubLevelName    string    `json:"level_name"`
	SubLevelCode    string    `json:"level_code"`
	Description     string    `json:"description"`
}

func (h Handler) GetSubLevels(c *fiber.Ctx) error {
	var subLevels []models.SubLevel

	if result := h.DB.Preload("Level").Find(&subLevels); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	return c.Status(fiber.StatusOK).JSON(&subLevels)
}

func (h Handler) GetSubLevel(c *fiber.Ctx) error {
	id := c.Params("id")

	var subLevel models.SubLevel

	if result := h.DB.Preload("Level").First(&subLevel, "id = ?", id); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	return c.Status(fiber.StatusOK).JSON(&subLevel)
}

func (h Handler) AddSubLevel(c *fiber.Ctx) error {
	body := SubLevelRequestBody{}

	// parse body, attach to SubLevelRequestBody struct
	if err := c.BodyParser(&body); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	var subLevel models.SubLevel

	subLevel.LevelID = body.LevelID
	subLevel.SubLevelName = body.SubLevelName
	subLevel.SubLevelCode = body.SubLevelCode
	subLevel.Description = body.Description
	subLevel.InstitutionCode = body.InstitutionCode

	// insert new db entry
	if result := h.DB.Create(&subLevel); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	return c.Status(fiber.StatusCreated).JSON(&subLevel)
}

func (h Handler) UpdateSubLevel(c *fiber.Ctx) error {
	id := c.Params("id")
	body := SubLevelRequestBody{}

	// getting request's body
	if err := c.BodyParser(&body); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	var subLevel models.SubLevel

	if result := h.DB.Preload("Level").First(&subLevel, "id = ?", id); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	subLevel.InstitutionCode = body.InstitutionCode
	subLevel.LevelID = body.LevelID
	subLevel.SubLevelName = body.SubLevelName
	subLevel.SubLevelCode = body.SubLevelCode
	subLevel.Description = body.Description

	// save level
	h.DB.Save(&subLevel)

	return c.Status(fiber.StatusOK).JSON(&subLevel)
}

func (h Handler) DeleteSubLevel(c *fiber.Ctx) error {
	id := c.Params("id")

	var subLevel models.SubLevel

	if result := h.DB.First(&subLevel, "id = ?", id); result.Error != nil {
		return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
	}

	// delete subLevel from db
	h.DB.Delete(&subLevel)

	return c.SendStatus(fiber.StatusOK)
}