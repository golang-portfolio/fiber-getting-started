package controllers

import (
	"github.com/google/uuid"
	"github.com/gofiber/fiber/v2"
    "github.com/Kengathua/fiberone/pkg/models"
)

type LevelRequestBody struct {
	InstitutionID		uuid.UUID	`json:"institution_id"`
	InstitutionCode		string		`json:"institution_code"`
    LevelName			string 		`json:"level_name"`
    LevelCode			string 		`json:"level_code"`
    Description 		string 		`json:"description"`
}

func (h Handler) GetLevels(c *fiber.Ctx) error {
    var levels []models.Level

    if result := h.DB.Preload("Institution").Find(&levels); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusOK).JSON(&levels)
}

func (h Handler) GetLevel(c *fiber.Ctx) error {
    id := c.Params("id")

    var level models.Level

    if result := h.DB.Preload("Institution").First(&level, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusOK).JSON(&level)
}

func (h Handler) AddLevel(c *fiber.Ctx) error {
    body := LevelRequestBody{}

    // parse body, attach to LevelRequestBody struct
    if err := c.BodyParser(&body); err != nil {
        return fiber.NewError(fiber.StatusBadRequest, err.Error())
    }

    var level models.Level

	level.InstitutionID = body.InstitutionID
    level.LevelName = body.LevelName
    level.LevelCode = body.LevelCode
    level.Description = body.Description
	level.InstitutionCode = body.InstitutionCode

    // insert new db entry
    if result := h.DB.Create(&level); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusCreated).JSON(&level)
}

func (h Handler) UpdateLevel(c *fiber.Ctx) error {
    id := c.Params("id")
    body := LevelRequestBody{}

    // getting request's body
    if err := c.BodyParser(&body); err != nil {
        return fiber.NewError(fiber.StatusBadRequest, err.Error())
    }

    var level models.Level

    if result := h.DB.Preload("Institution").First(&level, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

	level.InstitutionCode = body.InstitutionCode
	level.InstitutionID = body.InstitutionID
    level.LevelName = body.LevelName
    level.LevelCode = body.LevelCode
    level.Description = body.Description

    // save level
    h.DB.Save(&level)

    return c.Status(fiber.StatusOK).JSON(&level)
}

func (h Handler) DeleteLevel(c *fiber.Ctx) error {
    id := c.Params("id")

    var level models.Level

    if result := h.DB.First(&level, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    // delete level from db
    h.DB.Delete(&level)

    return c.SendStatus(fiber.StatusOK)
}