package controllers

import (
	"github.com/google/uuid"
	"github.com/gofiber/fiber/v2"
    "github.com/Kengathua/fiberone/pkg/models"
)

type ProgrammeRequestBody struct {
	InstitutionID		uuid.UUID	`json:"institution_id"`
	InstitutionCode		string		`json:"institution_code"`
    ProgrammeName		string 		`json:"programme_name"`
    ProgrammeCode		string 		`json:"programme_code"`
    Description 		string 		`json:"description"`
}

func (h Handler) GetProgrammes(c *fiber.Ctx) error {
    var programmes []models.Programme

    if result := h.DB.Preload("Institution").Find(&programmes); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusOK).JSON(&programmes)
}

func (h Handler) GetProgramme(c *fiber.Ctx) error {
    id := c.Params("id")

    var programme models.Programme

    if result := h.DB.Preload("Institution").First(&programme, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusOK).JSON(&programme)
}

func (h Handler) AddProgramme(c *fiber.Ctx) error {
    body := ProgrammeRequestBody{}

    // parse body, attach to ProgrammeRequestBody struct
    if err := c.BodyParser(&body); err != nil {
        return fiber.NewError(fiber.StatusBadRequest, err.Error())
    }

    var programme models.Programme

	programme.InstitutionID = body.InstitutionID
    programme.ProgrammeName = body.ProgrammeName
    programme.ProgrammeCode = body.ProgrammeCode
    programme.Description = body.Description
	programme.InstitutionCode = body.InstitutionCode

    // insert new db entry
    if result := h.DB.Create(&programme); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    return c.Status(fiber.StatusCreated).JSON(&programme)
}

func (h Handler) UpdateProgramme(c *fiber.Ctx) error {
    id := c.Params("id")
    body := ProgrammeRequestBody{}

    // getting request's body
    if err := c.BodyParser(&body); err != nil {
        return fiber.NewError(fiber.StatusBadRequest, err.Error())
    }

    var programme models.Programme

    if result := h.DB.Preload("Institution").First(&programme, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

	programme.InstitutionCode = body.InstitutionCode
	programme.InstitutionID = body.InstitutionID
    programme.ProgrammeName = body.ProgrammeName
    programme.ProgrammeCode = body.ProgrammeCode
    programme.Description = body.Description

    // save programme
    h.DB.Save(&programme)

    return c.Status(fiber.StatusOK).JSON(&programme)
}

func (h Handler) DeleteProgramme(c *fiber.Ctx) error {
    id := c.Params("id")

    var programme models.Programme

    if result := h.DB.First(&programme, "id = ?", id); result.Error != nil {
        return fiber.NewError(fiber.StatusNotFound, result.Error.Error())
    }

    // delete programme from db
    h.DB.Delete(&programme)

    return c.SendStatus(fiber.StatusOK)
}