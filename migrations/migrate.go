package main

import (
	"github.com/Kengathua/fiberone/pkg/config"
	"github.com/Kengathua/fiberone/pkg/models"
)

func init() {
	config.ConnectToDb()
}

func main() {
	// CREATE EXTENSION IF NOT EXISTS "uuid-ossp"
	config.DB.AutoMigrate(&models.Institution{})
	config.DB.AutoMigrate(&models.School{})
	config.DB.AutoMigrate(&models.Programme{})
	config.DB.AutoMigrate(&models.Level{})
	config.DB.AutoMigrate(&models.SubLevel{})
}