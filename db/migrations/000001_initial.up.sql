BEGIN;

CREATE TABLE IF NOT EXISTS platform_institution (
    id uuid NOT NULL PRIMARY KEY,
    created timestamp WITH time zone NOT NULL,
    updated timestamp WITH time zone NOT NULL,
    deleted_at timestamp WITH time zone
    created_by uuid,
    updated_by uuid,
    institution_code integer NOT NULL UNIQUE,
    institution_name varchar(100) NOT NULL UNIQUE,
    is_active boolean NOT NULL,
    description text,
);

CREATE TABLE IF NOT EXISTS platform_institution_contact (
    id uuid NOT NULL PRIMARY KEY,
    created timestamp WITH time zone NOT NULL,
    updated timestamp WITH time zone NOT NULL,
    deleted_at timestamp WITH time zone
    created_by uuid,
    updated_by uuid,

    institution_id uuid NOT NULL CONSTRAINT platform_institution_contact_institution_id_e7321b28_fk_platform_institution_id REFERENCES platform_institution deferrable initially deferred,

    institution_name varchar(100) NOT NULL UNIQUE,
    is_active boolean NOT NULL,
    email_address varchar(100) NOT NULL,
    phone_number varchar(128) NOT NULL,
    description text,
    postal_address varchar(100) NOT NULL,
    physical_address text NOT NULL,
    default_country varchar(255) NOT NULL,
    deleted_at timestamp WITH time zone
);

COMMIT;
