package main

import "github.com/gofiber/fiber/v2"
import "github.com/Kengathua/fiberone/pkg/routes"
import "github.com/Kengathua/fiberone/pkg/config"

func main() {
	app := fiber.New()
    config.ConnectToDb()

	api := app.Group("/api")

	v1 := api.Group("/v1", func(c *fiber.Ctx) error { // middleware for /api/v1/platform
        c.Set("Version", "v1")
        return c.Next()
    })

	platformUrl := v1.Group("platform", func(c *fiber.Ctx) error { // middleware for /api/v1
        c.Set("Version", "v1")
        return c.Next()
    })

    routes.RegisterRoutes(platformUrl, config.DB) // /api/v1/platform

	app.Listen(":3000")
}